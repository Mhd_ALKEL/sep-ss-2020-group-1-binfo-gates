Summary about the project:
This is the repository of the web application BINFO Gates.
The Kanban boards could be found on: 
https://ds-git.fstc.uni.lu/software-engineering-project-ss2020/group1/boards
The documents, presentations, meeting minutes can only be found on this repository on “Documents” directory.
The application runs on Docker platform, but it is not fully optimized on Linux or with different aspect ratios like 16:10 or so.
The Docker image in this repository is imported from Web Programming course of the 5th semester.
We used an online database for portability and compatibility reasons. The file dbconfig.php contains the different configurations of it, it can be changed for using a local database on Docker for example(May cause some problems with permissions and ports mapping)
The images, backgrounds, photos are all local in Images directory in AppCode directory.
We have a video of this application (To show animation with some further explanations) you can watch it here.
https://drive.google.com/file/d/1c68iNIT9uteeMRXc3DxiFgQPTUc64qza/view
  

