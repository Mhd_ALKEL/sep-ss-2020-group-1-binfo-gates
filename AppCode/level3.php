<?php
include 'gameSession.php';

$level = 3;


if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    //endGame();
    $gameSession = retrieveGameSession();
    $gameSession->startLevel($level);
} else {

    $accuracy = 0;
    submitScore($level, $accuracy, "");
}

?>
<!DOCTYPE HTML>
<html>

<head>
    <title>Level 3</title>
    <link href="template.css" rel="stylesheet" type="text/css">
    <style>
        body {
            background-image: url('Images/Computer.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100% 100%;
        }

        .content {
            position: absolute;
            top: 64%;
            left: 43%;
            height: 22%;
            width: 25%;
            background-color: transparent;
            color: azure;

            font-size: 40px;
        }

        .images {
            position: absolute;
            top: 36%;
            left: 34%;
            height: 20%;
            width: 38%;

        }

        .Question {
            position: absolute;
            top: 10%;
            left: 25%;
            text-align: center;
            color: azure;
            font-family: "Book Antiqua";
            font-size: 35px;
        }
    </style>
    <script>
        let already = false;
        let accuracy = 0;

        function allowDrop(ev) {
            if (already === false) {
                ev.preventDefault();
            }
        }

        function drag(ev) {
            ev.dataTransfer.setData("text", ev.target.id);
        }

        function drop(ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
            ev.target.appendChild(document.getElementById(data));
            already = true;
            if (document.getElementById(data).id === "i3") {
                accuracy = 120;
            }
        }

        function always(ev) {
            ev.preventDefault();
        }

        function alwaysDrop(ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
            ev.target.appendChild(document.getElementById(data));
            already = false;
            accuracy = 0;
        }

        function endLevel() {
            window.location.replace('submitScore.php?level=3&accuracy=' + accuracy)
        }

        function alerter() {
            if (alert('The right image is: \nthe Third one \nwhich contains #include in the first line')) {
                zeroing();
            } else {
                zeroing();
            }
        }

        function zeroing() {
            accuracy = 0;
            window.location.replace('submitScore.php?level=3&accuracy=' + accuracy + '&message=You used helping button so your points here are zero')
        }
    </script>
</head>

<body>
    <div class="Question"> Choose the code that written in C language<br> and drag it from the Main screen to your laptop screen:</div>

    <div id="div1" class="content" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
    <div class="images" ondrop="alwaysDrop(event)" ondragover="always(event)">
        <img id="i1" src="Images/PhpCode.jpg" draggable="true" ondragstart="drag(event)" height="100" width="100">

        <img id="i2" src="Images/JavaCode.jpg" draggable="true" ondragstart="drag(event)" height="130" width="200">

        <img id="i3" src="Images/CCode.jpg" draggable="true" ondragstart="drag(event)" height="120" width="140">

        <img id="i4" src="Images/JsCode.jpg" draggable="true" ondragstart="drag(event)" height="100" width="100">

    </div>

    <div class="vp"> <button onclick="endLevel()"><img src="Images/Level1/NextLevelSign.png">
        </button></div>

    <div class="helpText">
        HELP!
    </div>
    <div class="thumb">
        <a href="#">
            <span><img src="Images/Level1/PuzzlesTemplatesQM.png" onclick="alerter()"></span>
        </a>
    </div>
    <div class="challenge">
        <span>Programming<br>Challenge by Professor: <br>Steﬀen Rothkugel</span>
    </div>
    <div class="levelNumber">
        &nbsp;Level 03&nbsp;
    </div>
</body>

</html>