<?php

include 'gameSession.php';

$level = 4;


if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    //endGame();
    $gameSession = retrieveGameSession();
    $gameSession->startLevel($level);
} else {

    $accuracy = 0;
    submitScore($level, $accuracy, "");
}

?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Level 4</title>
    <link href="template.css" rel="stylesheet" type="text/css">
    <style>
        body {
            background-image: url('Images/OsLevel.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100% 100%;
        }
        .shell{
            position: absolute;
            top: 2%;
            left: 25%;
            height: 70%;
            width: 60%;
            color: #f4fcac;
            font-size: 32px;
            font-family: "Adobe Arabic";
            background-image: url("Images/Level4/linuxCont.png");
            background-size: auto;
        }
        .longList{
            position: absolute;
            top: 74%;
            left: 27%;
            height: 20%;
            width: 65%;
        }
        .container{
            position: absolute;
            top: 35%;
            left: 2%;
            height: 55%;
            width: 100%;
        }
    </style>
    <script>
        let accuracy = 0;
        function allowDrop(ev) {
            ev.preventDefault();
        }
        function drag(ev) {
            ev.dataTransfer.setData("text", ev.target.id);
        }
        function drop(ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
            ev.target.appendChild(document.getElementById(data));
            if(document.getElementById(data).id === "i2"){
                accuracy += 30;
            }
            if(document.getElementById(data).id === "i3"){
                accuracy += 30;
            }
            if(document.getElementById(data).id === "i5"){
                accuracy += 30;
            }
            if(document.getElementById(data).id === "i7"){
                accuracy += 30;
            }
        }
        function reversedDrop(ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
            ev.target.appendChild(document.getElementById(data));
            if(document.getElementById(data).id === "i2"){
                accuracy -= 30;
            }
            if(document.getElementById(data).id === "i3"){
                accuracy -= 30;
            }
            if(document.getElementById(data).id === "i5"){
                accuracy -= 30;
            }
            if(document.getElementById(data).id === "i7"){
                accuracy -= 30;
            }
        }
        function endLevel() {
            window.location.replace('submitScore.php?level=4&accuracy=' + accuracy)
        }
        function alerter(){
            if (alert('The right answers are: \ndebian \nfedora "The blue logo with white f" \nredHat \nubuntu "The red circle "')) {
                zeroing();
            }
            else{
                zeroing();
            }
        }
        function zeroing() {
            accuracy = 0;
            window.location.replace('submitScore.php?level=4&accuracy=' + accuracy + '&message=You used helping button so your points here are zero');
        }
    </script>
</head>
<body>
<div class="shell"><br><br><br>&nbsp;
    Move only the Logos of Linux distributions to this shell window.
    <div class="container" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
</div>
<div class="longList" ondrop="reversedDrop(event)" ondragover="allowDrop(event)">
    <img id="i1" src="Images/Level4/1+logo.png" draggable="true" ondragstart="drag(event)" height="100" width="100">
    <img id="i2" src="Images/Level4/debianLinux.png" draggable="true" ondragstart="drag(event)"  height="100" width="100">
    <img id="i3" src="Images/Level4/fedoraLinux.png" draggable="true" ondragstart="drag(event)"  height="100" width="100">
    <img id="i4" src="Images/Level4/PieLogo.png" draggable="true" ondragstart="drag(event)"  height="100" width="100">
    <img id="i5" src="Images/Level4/redhatLinux.png" draggable="true" ondragstart="drag(event)"  height="100" width="100">
    <img id="i6" src="Images/Level4/Rlogo.png" draggable="true" ondragstart="drag(event)"  height="100" width="100">
    <img id="i7" src="Images/Level4/ubuntuLinux.png" draggable="true" ondragstart="drag(event)"  height="100" width="100">
    <img id="i8" src="Images/Level4/WINntLogo.png" draggable="true" ondragstart="drag(event)"  height="100" width="100">
</div>
<div class="vp" > <button onclick="endLevel()"><img src="Images/Level1/NextLevelSign.png">
    </button></div>
<div class="helpText">
    HELP!
</div>
<div class="thumb">
    <a href="#">
        <span><img src="Images/Level1/PuzzlesTemplatesQM.png" onclick="alerter()"></span>
    </a>
</div>
<div class="challenge">
    <span>Operating Systems<br>Challenge by Professor: <br>Christian GREVISSE</span>
</div>
<div class="levelNumber">
    &nbsp;Level 04&nbsp;
</div>
</body>
</html>