<?php

include 'gameSession.php';

$level = 7;


if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    //endGame();
    $gameSession = retrieveGameSession();
    $gameSession->startLevel($level);
} else {

    $accuracy = 0;
    submitScore($level, $accuracy, "");
}

?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Level 7</title>
    <link href="template.css" rel="stylesheet" type="text/css">
    <style>
        body {
            background-image: url('Images/AlgoLevel.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100% 100%;
        }
        .tableLocation{
            position: absolute;
            top: 5%;
            left: 25%;
            height: 85%;
            width: 70%;
        }
        table{
            border-style: solid;
            border-width: 7px;
            border-color: black;
        }
        td{
            border-width: 0px;
            border-color: black;
            width: 80px;
            height: 80px;
        }
        .black{
            background: grey;
        }
        .white{
            background: azure;
        }
        .disc{
            position: absolute;
            top: 10%;
            left: 70%;
            height: 35%;
            width: 27%;
            border-style: solid;
            border-width: 3px;
            border-color: black;
            background-color: seagreen;
            font-family: Pristina;
            font-size: 25px;
            color: white;
        }
    </style>
    <script>
        let clicked2 = false;
        let clicked3 = false;
        let clicked4 = false;
        let clicked5 = false;
        let clicked7 = false;
        let accuracy = 0;
        let chosen2 = false;
        let chosen3 = false;
        let chosen4 = false;
        let chosen5 = false;
        let chosen7 = false;
        function generate_DestroyQueenLine2(id) {
            let name = id + "i";
            if(clicked2 === false){
            let img = document.createElement("img");
            img.id = id + "i";
            img.src = "Images/chessQueen.png";
            img.height = 75;
            img.width = 75;
            document.getElementById(id).appendChild(img);
            clicked2 = true;
            if(id === "B2"){
                chosen2 = true;
                accuracy +=24;
            }
            }
            else{
           let block = document.getElementById(name);
           block.remove();
           clicked2 = false;
           if(id === "B2" && chosen2 === true){
                accuracy -=24;
                chosen2 = false;
           }
            }
            }
        function generate_DestroyQueenLine3(id) {
            let name = id + "i";
            if(clicked3 === false) {
                let img = document.createElement("img");
                img.id = id + "i";
                img.src = "Images/chessQueen.png";
                img.height = 75;
                img.width = 75;
                document.getElementById(id).appendChild(img);
                clicked3 = true;
                if (id === "C4") {
                    chosen3 = true;
                    accuracy += 24;}
                }
            else {
                    let block = document.getElementById(name);
                    block.remove();
                    clicked3 = false;
                    if (id === "C4" && chosen3 === true) {
                        accuracy -= 24;
                        chosen3 = false;
                    }
                }
            }
        function generate_DestroyQueenLine4(id) {
            let name = id + "i";
            if(clicked4 === false){
                let img = document.createElement("img");
                img.id = id + "i";
                img.src = "Images/chessQueen.png";
                img.height = 75;
                img.width = 75;
                document.getElementById(id).appendChild(img);
                clicked4 = true;
                if(id === "D1"){
                    chosen4 = true;
                    accuracy +=24;}
            }
            else{
                let block = document.getElementById(name);
                block.remove();
                clicked4 = false;
                    if(id === "D1" && chosen4 === true){
                        accuracy -=24;
                        chosen4 = false;}
            }
        }
        function generate_DestroyQueenLine5(id) {
            let name = id + "i";
            if(clicked5 === false){
                let img = document.createElement("img");
                img.id = id + "i";
                img.src = "Images/chessQueen.png";
                img.height = 75;
                img.width = 75;
                document.getElementById(id).appendChild(img);
                clicked5 = true;
                if(id === "E7"){
                    chosen5 = true;
                    accuracy +=24;}
            }
            else{
                let block = document.getElementById(name);
                block.remove();
                clicked5 = false;
                    if(id === "E7" && chosen5 === true){
                        accuracy -=24;
                        chosen4 = false;}
            }
        }
        function generate_DestroyQueenLine7(id) {
            let name = id + "i";
            if(clicked7 === false){
                let img = document.createElement("img");
                img.id = id + "i";
                img.src = "Images/chessQueen.png";
                img.height = 75;
                img.width = 75;
                document.getElementById(id).appendChild(img);
                clicked7 = true;
                if(id === "G3"){
                    chosen7 = true;
                    accuracy +=24;}
            }
            else{
                let block = document.getElementById(name);
                block.remove();
                clicked7 = false;
                    if(id === "G3" && chosen7 === true){
                        accuracy -=24;
                        chosen7 = false;}
            }
        }
        function endLevel() {
            window.location.replace('submitScore.php?level=7&accuracy=' + accuracy)
        }
        function alerter(){
            if (alert('The Answer will be on the form \n [   .    .    .    .    .    .    .    *   ] \n' +
                ' [   .    *    .    .    .    .    .    .   ] \n [   .    .    .    *    .    .    .    .   ] \n ' +
                '[   *    .    .    .    .    .    .    .   ] \n [   .    .    .    .    .    .    *    .   ] \n ' +
                '[   .    .    .    .    *    .    .    .   ] \n [   .    .    *    .    .    .    .    .   ] \n ' +
                '[   .    .    .    .    .    *    .    .   ]')) {
                zeroing();
            }
            else{
                zeroing();
            }
        }
        function zeroing() {
            accuracy = 0;
            window.location.replace('submitScore.php?level=7&accuracy=' + accuracy + '&message=You used helping button so your points here are zero');
        }
    </script>
</head>
<body>
<div class="disc">This is "8 Queens problem" in algorithms, We initially have 3 queens, so you need to add 5 more queens to the chessboard
to add a queen just click on the square that you want, to remove queen click again on the square<br><br>Note that you can't add more than
queen at the same row.</div>
<div class="tableLocation"><table>
        <tr>
            <td class="black" id="A1"></td>
            <td class="white" id="A2"></td>
            <td class="black" id="A3"></td>
            <td class="white" id="A4"></td>
            <td class="black" id="A5"></td>
            <td class="white" id="A6"></td>
            <td class="black" id="A7"></td>
            <td class="white" id="A8"><img id="A8i" src="Images/chessQueen.png" height="75" width="75" ></td>
        </tr>
        <tr>
            <td class="white" id="B1" onclick="generate_DestroyQueenLine2(this.id)"></td>
            <td class="black" id="B2" onclick="generate_DestroyQueenLine2(this.id)"></td>
            <td class="white" id="B3" onclick="generate_DestroyQueenLine2(this.id)"></td>
            <td class="black" id="B4" onclick="generate_DestroyQueenLine2(this.id)"></td>
            <td class="white" id="B5" onclick="generate_DestroyQueenLine2(this.id)"></td>
            <td class="black" id="B6" onclick="generate_DestroyQueenLine2(this.id)"></td>
            <td class="white" id="B7" onclick="generate_DestroyQueenLine2(this.id)"></td>
            <td class="black" id="B8" onclick="generate_DestroyQueenLine2(this.id)"></td>
        </tr>
        <tr>
            <td class="black" id="C1" onclick="generate_DestroyQueenLine3(this.id)"></td>
            <td class="white" id="C2" onclick="generate_DestroyQueenLine3(this.id)"></td>
            <td class="black" id="C3" onclick="generate_DestroyQueenLine3(this.id)"></td>
            <td class="white" id="C4" onclick="generate_DestroyQueenLine3(this.id)"></td>
            <td class="black" id="C5" onclick="generate_DestroyQueenLine3(this.id)"></td>
            <td class="white" id="C6" onclick="generate_DestroyQueenLine3(this.id)"></td>
            <td class="black" id="C7" onclick="generate_DestroyQueenLine3(this.id)"></td>
            <td class="white" id="C8" onclick="generate_DestroyQueenLine3(this.id)"></td>
        </tr>
        <tr>
            <td class="white" id="D1" onclick="generate_DestroyQueenLine4(this.id)"></td>
            <td class="black" id="D2" onclick="generate_DestroyQueenLine4(this.id)"></td>
            <td class="white" id="D3" onclick="generate_DestroyQueenLine4(this.id)"></td>
            <td class="black" id="D4" onclick="generate_DestroyQueenLine4(this.id)"></td>
            <td class="white" id="D5" onclick="generate_DestroyQueenLine4(this.id)"></td>
            <td class="black" id="D6" onclick="generate_DestroyQueenLine4(this.id)"></td>
            <td class="white" id="D7" onclick="generate_DestroyQueenLine4(this.id)"></td>
            <td class="black" id="D8" onclick="generate_DestroyQueenLine4(this.id)"></td>
        </tr>
        <tr>
            <td class="black" id="E1" onclick="generate_DestroyQueenLine5(this.id)"></td>
            <td class="white" id="E2" onclick="generate_DestroyQueenLine5(this.id)"></td>
            <td class="black" id="E3" onclick="generate_DestroyQueenLine5(this.id)"></td>
            <td class="white" id="E4" onclick="generate_DestroyQueenLine5(this.id)"></td>
            <td class="black" id="E5" onclick="generate_DestroyQueenLine5(this.id)"></td>
            <td class="white" id="E6" onclick="generate_DestroyQueenLine5(this.id)"></td>
            <td class="black" id="E7" onclick="generate_DestroyQueenLine5(this.id)"></td>
            <td class="white" id="E8" onclick="generate_DestroyQueenLine5(this.id)"></td>
        </tr>
        <tr>
            <td class="white" id="F1"></td>
            <td class="black" id="F2"></td>
            <td class="white" id="F3"></td>
            <td class="black" id="F4"></td>
            <td class="white" id="F5"><img src="Images/chessQueen.png" height="75" width="75"></td>
            <td class="black" id="F6"></td>
            <td class="white" id="F7"></td>
            <td class="black" id="F8"></td>
        </tr>
        <tr>
            <td class="black" id="G1" onclick="generate_DestroyQueenLine7(this.id)"></td>
            <td class="white" id="G2" onclick="generate_DestroyQueenLine7(this.id)"></td>
            <td class="black" id="G3" onclick="generate_DestroyQueenLine7(this.id)"></td>
            <td class="white" id="G4" onclick="generate_DestroyQueenLine7(this.id)"></td>
            <td class="black" id="G5" onclick="generate_DestroyQueenLine7(this.id)"></td>
            <td class="white" id="G6" onclick="generate_DestroyQueenLine7(this.id)"></td>
            <td class="black" id="G7" onclick="generate_DestroyQueenLine7(this.id)"></td>
            <td class="white" id="G8" onclick="generate_DestroyQueenLine7(this.id)"></td>
        </tr>
        <tr>
            <td class="white" id="H1"></td>
            <td class="black" id="H2"></td>
            <td class="white" id="H3"></td>
            <td class="black" id="H4"></td>
            <td class="white" id="H5"></td>
            <td class="black" id="H6"><img src="Images/chessQueen.png" height="75" width="75"></td>
            <td class="white" id="H7"></td>
            <td class="black" id="H8"></td>
        </tr>
    </table>
</div>
<div class="vp" > <button onclick="endLevel()"><img src="Images/Level1/NextLevelSign.png">
    </button></div>
<div class="helpText">
    HELP!
</div>
<div class="thumb">
    <a href="#">
        <span><img src="Images/Level1/PuzzlesTemplatesQM.png" onclick="alerter()"></span>
    </a>
</div>
<div class="challenge">
    <span>Algorithms<br>Challenge by Professor: <br>Pierre Kelsen</span>
</div>
<div class="levelNumber">
    &nbsp;Level 07&nbsp;
</div>
</body>
</html>