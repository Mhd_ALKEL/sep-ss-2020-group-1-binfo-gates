<?php
include 'gameSession.php';

// set the number of your variable here
$level = 5;

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    //endGame();
    $gameSession = retrieveGameSession();
    $gameSession->startLevel($level);
} else {
    try {
        $accuracy = 0;
        $splittedAnswer = explode(",", $_POST['answer']);
        for ($i = 0; $i < count($splittedAnswer); $i++) {
            if (strval($i + 1) == $splittedAnswer[$i]) {
                $accuracy += 20;
            }
        }
    } catch (Exception $e) {
        echo "cheater";
        return;
    }

    submitScore($level, $accuracy, "");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="puzzle" content="width=device-width, initial-scale=1.0">
    <title>Level 5</title>
    <!-- development version, includes helpful console warnings -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/vue/2.5.2/vue.min.js"></script>
    <!-- CDNJS :: Sortable (https://cdnjs.com/) -->
    <script src="//cdn.jsdelivr.net/npm/sortablejs@1.8.4/Sortable.min.js"></script>
    <!-- CDNJS :: Vue.Draggable (https://cdnjs.com/) -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.20.0/vuedraggable.umd.min.js"></script>
    <link href="template.css" rel="stylesheet" type="text/css">

    <style>
        body {
            background-image: url("Images/wall.jpg");
            background-size: cover;
            overflow: auto;
            width: 100%;
            height: 100%;
            margin: 0 auto;
            position: static;
        }
        .ide {
            position: absolute;
            top: 13%;
            left: 57%;
            height: 600px;
            width: 400px;
            background-image: url("Images/paper.png");
            background-size: cover;
        }
        .graph{
            position: absolute;
            top: 15%;
            left: 60%;
            height: 300px;
            width: 300px;
            background-image: url("Images/graph.png");
            background-size: contain;
            background-repeat: no-repeat;
        }
        .content {
            position: absolute;
            top: 55%;
            left: 59%;
            color: #afb1b3;
            display: flex;
            height: 100px;
            width: 300px;
            font-family: "Century";
            font-size: 20px;
            line-height: 30px;
        }
        .codeBlocks {
            position: absolute;
            background-color: #1a2c16;
            top: 22%;
            left: 8%;
            height: 420px;
            width: 500px;
            border-style: groove;
            border-color: #000000;
            font-family: "Century";
            font-size: 28px;
            color: #afb1b3;
            text-align: center;
            alignment: center;
        }
        .smallbox {
            height: 60px;
            width: 300px;
            display: flex;
            color: azure;
            position: absolute;
            top: 63%;
            left: 5%;
        }
        .items {
            border-color: transparent;
            border-style: solid;
            border-width: 10px;
            display: flex;
            justify-content: center;
            align-items: center;
        }
    </style>
</head>
<script>
    function alerter() {
        if (alert('The right order is: d/dx x^3 -3x = 3x^2 -3')) {
            zeroing();
        } else {
            zeroing();
        }
    }

    function zeroing() {
        let accuracy = 0;
        window.location.replace('submitScore.php?level=5&accuracy=' + accuracy + '&message=You used helping button so your points here are zero');
    }
</script>

<body class="body">
    <div class="ide">
    </div>
    <div class="graph"></div>
    <div id="app">
        <div class="codeBlocks">
            You have an unsorted mathematical elements here<br>You need to drag the elements to the sheet on the right.
            <br>And then you need to make it a correct derivative equation<br>The graph on the sheet may help you.<br>
            ----------------------
            <draggable class="smallbox" :list="toSolve" group="code">
                <div class="list-group-item" v-for="item in toSolve" :key="item.id">
                    <div class="items"><img v-bind:src="item.text" alt="" height="60" width="60"></div>
                </div>
            </draggable>
        </div>
        <div>
            <draggable class="content" :list="attempt" group="code" @change="onUpdate">
                <div class="list-group-item" v-for="item in attempt" :key="item.id">
                    <img v-bind:src="item.text" alt="" height="60" width="60">
                </div>
            </draggable>
        </div>
        <form action="level5.php" method="POST">
            <input type="text" name="answer" id="answer" hidden="true">
            <button class="vp"> <img src="Images/Level1/NextLevelSign.png"> </button>
        </form>
    </div>
    <div class="helpText">
        HELP!
    </div>
    <div class="thumb">
        <a href="#">
            <span><img src="Images/Level1/PuzzlesTemplatesQM.png" onclick="alerter()"></span>
        </a>
    </div>
    <div class="challenge">
        <span>Calculus<br>Challenge by Professor: <br>Franck Leprévost</span>
    </div>
    <div class="levelNumber">
        &nbsp;Level 05&nbsp;
    </div>
    <script src="scripts/level5.js" type="module">
    </script>
</body>
</html>