<?php

include 'gameSession.php';

// set the number of your variable here
$level = 8;

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    //endGame();
    $gameSession = retrieveGameSession();
    $gameSession->startLevel($level);
} else {
    try {        
        $accuracy = 0;
        if ($_POST['answer1'] == "1,2") {
            $accuracy += 60;
        }
        if ($_POST['answer2'] == "5,6,7") {
            $accuracy += 60;
        }

        submitScore($level, $accuracy, "");
    } catch (Exception $e) {
        echo "cheater";
        return;
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Level 8</title>
    <!-- development version, includes helpful console warnings -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/vue/2.5.2/vue.min.js"></script>
    <!-- CDNJS :: Sortable (https://cdnjs.com/) -->
    <script src="//cdn.jsdelivr.net/npm/sortablejs@1.8.4/Sortable.min.js"></script>
    <!-- CDNJS :: Vue.Draggable (https://cdnjs.com/) -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.20.0/vuedraggable.umd.min.js"></script>
    <link href="template.css" rel="stylesheet" type="text/css">

    <style>
        body {
            background-image: url("Images/pointers.png");
            background-size: initial;
            overflow: auto;
            width: 100%;
            height: 100%;
        }

        .ide1 {
            position: absolute;
            top: 28%;
            left: 50%;
            height: 297px;
            width: 268px;
            background-size: cover;
            border-style: groove;
            border-color: black;
            background-image: url("Images/Level12/q1.PNG");
            background-size: contain;
            background-repeat: no-repeat;
        }

        .ide2 {
            position: absolute;
            top: 20%;
            left: 69%;
            height: 418px;
            width: 300px;
            background-size: cover;
            border-style: groove;
            border-color: black;
            background-image: url("Images/Level12/q2.PNG");
            background-size: contain;
            background-repeat: no-repeat;
        }

        .firstContent {
            position: absolute;
            top: 20%;
            left: 17%;
            color: #afb1b3;
            height: 100px;
            width: 100px;
            font-family: "Century";
            font-size: 18px;
            line-height: 25px;
        }

        .secondContent {
            position: absolute;
            top: 27%;
            left: 19%;
            color: #afb1b3;
            height: 100px;
            width: 100px;
            font-family: "Century";
            font-size: 18px;
            line-height: 25px;
        }

        .codeBlocks {
            position: absolute;
            background-color: #1a2c16;
            top: 18%;
            left: 10%;
            height: 500px;
            width: 600px;
            border-style: groove;
            border-color: #000000;
            font-family: "Century";
            font-size: 20px;
            color: #afb1b3;
            text-align: center;
            alignment: center;
        }

        .smallbox {
            height: 60px;
            width: 150px;
            color: #48a0dc;
            position: absolute;
            top: 22%;
            left: 37%;
            font-size: 20px;
        }

        .items {
            border-color: transparent;
            border-style: solid;
            border-width: 10px;
            justify-content: center;
            align-items: center;
        }
    </style>
    <script>
        function alerter() {
            if (alert('The right order answers: \nFirst code: \np = q; \n*p = 2; \nSecond code: \nchar *t = x; \n' +
                    'x = y; \ny = t;')) {
                zeroing();
            } else {
                zeroing();
            }
        }

        function zeroing() {
            let accuracy = 0;
            window.location.replace('submitScore.php?level=8&accuracy=' + accuracy + '&message=You used helping button so your points here are zero');
        }
    </script>
</head>

<body>
    <div id="app">
        <div class="codeBlocks">Drag the pieces of code to the second side. <br>First window should give an output 0 2
            <br> Second Window should give an output programing binfo <br> Note that there are wrong lines you must avoid to choose
            <draggable class="smallbox" :list="toSolve" group="code">
                <div class="list-group-item" v-for="item in toSolve" :key="item.id">
                    <div class="items">{{ item.text }}</div>
                </div>
            </draggable>
        </div>
        <div class="ide1">
            <label for="q1Descriptor"></label> <br>
            <draggable class="firstContent" :list="attempt1" group="code" @change="onUpdate1">
                <div class="list-group-item" v-for="item in attempt1" :key="item.id">
                    {{ item.text }}
                </div>
            </draggable>
        </div>
        <div class="ide2">
            <label for="q2Descriptor"></label> <br>
            <draggable class="secondContent" :list="attempt2" group="code" @change="onUpdate2">
                <div class="list-group-item" v-for="item in attempt2" :key="item.id">
                    {{ item.text }}
                </div>
            </draggable>
        </div>
        <form action="level8.php" method="POST">
            <input type="text" name="answer" id="answer" hidden="true">
            <button class="vp"> <img src="Images/Level1/NextLevelSign.png"> </button>
        </form>
    </div>
    <div class="helpText">
        HELP!
    </div>
    <div class="thumb">
        <a href="#">
            <span><img src="Images/Level1/PuzzlesTemplatesQM.png" onclick="alerter()"></span>
        </a>
    </div>
    <div class="challenge">
        <span>C Programming<br>Challenge by Professor: <br>Steﬀen Rothkugel</span>
    </div>
    <div class="levelNumber">
        &nbsp;Level 08&nbsp;
    </div>
    <script src="scripts/level12.js" type="module">
    </script>
</body>

</html>