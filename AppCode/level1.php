<?php

include 'gameSession.php';

$accuracy = 0;
function calculate($value)
{
    $accuracy = $value;
    if ($_POST["o1"] == 3) {
        $accuracy += 6.67;
    }
    if ($_POST["o2"] == 7) {
        $accuracy += 6.67;
    }
    if ($_POST["o3"] == 4) {
        $accuracy += 6.67;
    }
    if ($_POST["o4"] == 4) {
        $accuracy += 6.67;
    }
    if ($_POST["b1"] == 1) {
        $accuracy += 6.67;
    }
    if ($_POST["b2"] == 1) {
        $accuracy += 6.67;
    }
    if ($_POST["b3"] == 1) {
        $accuracy += 6.67;
    }
    if ($_POST["b4"] == 1) {
        $accuracy += 6.67;
    }
    if ($_POST["b5"] == 1) {
        $accuracy += 6.67;
    }
    if ($_POST["b6"] == 1) {
        $accuracy += 6.67;
    }
    if ($_POST["b7"] == 0) {
        $accuracy += 6.67;
    }
    if ($_POST["b8"] == 0) {
        $accuracy += 6.67;
    }
    if ($_POST["b9"] == 1) {
        $accuracy += 6.67;
    }
    if ($_POST["b10"] == 0) {
        $accuracy += 6.67;
    }
    if ($_POST["b11"] == 0) {
        $accuracy += 6.67;
    }
    if ($_POST["h1"] == 7) {
        $accuracy += 6.67;
    }
    if ($_POST["h2"] == "E") {
        $accuracy += 6.67;
    }
    if ($_POST["h3"] == 4) {
        $accuracy += 6.67;
    }
    return $accuracy;
}
$level = 1;

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $gameSession = retrieveGameSession();
    $gameSession->startLevel($level);
} else {
    $accuracy = calculate($accuracy);
    submitScore($level, $accuracy, "");
}

?>
<!Doctype HTML>
<html>

<head>
    <title>Level1</title>
    <link href="template.css" rel="stylesheet" type="text/css">
    <style>
        html,
        body {
            padding: 0;
            border: 0;
            outline: 0;
            overflow: auto;
            width: 100%;
            height: 100%;
            margin: 0 auto;
            position: static;
            display: contents;
            min-width: 1520px;
            min-height: 740px;
            vertical-align: baseline;
            background: transparent;
        }

        .bg {
            height: 100%;
            width: 100%;
            position: static;
            min-width: 1520px;
            min-height: 740px;
            background-image: url("Images/Level1/Level1P.jpg");
            -webkit-background-clip: border-box;
            -webkit-animation: movement 10s linear infinite;
            -webkit-backface-visibility: hidden;
        }

        @keyframes movement {
            50% {
                background-position: -50%;
            }
        }

        .content {
            position: absolute;
            top: 16%;
            left: 27%;
            height: 62%;
            width: 65%;
            border-style: groove;
            border-color: black;
            background-color: black;
            color: azure;
            font-family: "Kozuka Gothic Pro B";
            font-size: 40px;
        }

        .texts {
            font-family: "digital";
            font-size: 40px;
            text-align: center;
            background-color: black;
            color: aqua;
            -moz-appearance: textfield;
        }

        .InitialTexts {
            font-family: "digital";
            font-size: 40px;
            text-align: center;
            background-color: black;
            color: seagreen;
        }

        .smallList {
            font-size: 15px;
            align-content: center;
        }

        vp {
            position: absolute;
            bottom: 1.8%;
            right: 1.8%;
            border-style: groove;
            border-color: darkgrey;
            border-width: 5px;
        }

        .vp input {
            transition: transform .5s ease-in-out;
        }

        .vp:hover input {
            transform: scale(1) rotate(25deg);
        }
    </style>
    <script>
        function alerter() {
            if (alert('The answers: \nBinary:  11111100100 \nOctal: 3744 \nHexadecimal: 7E4')) {
                zeroing();
            } else {
                zeroing();
            }
        }

        function zeroing() {
            let accuracy = 0;
            window.location.replace('submitScore.php?level=1&accuracy=' + accuracy + '&message=You used helping button so your points here are zero');
        }
    </script>
</head>

<body>

    <form method="post" action="level1.php">
        <div class="content">
            Convert this number to the following systems<br>
            <label> Decimal: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </label>

            <input class="InitialTexts" value="2" maxlength="1" size="1" readonly />
            <input class="InitialTexts" value="0" maxlength="1" size="1" readonly />
            <input class="InitialTexts" value="2" maxlength="1" size="1" readonly />
            <input class="InitialTexts" value="0" maxlength="1" size="1" readonly />
            <br><label>Octal: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </label>
            <select name="o1" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
                <option value="2" class="smallList">2</option>
                <option value="3" class="smallList">3</option>
                <option value="4" class="smallList">4</option>
                <option value="5" class="smallList">5</option>
                <option value="6" class="smallList">6</option>
                <option value="7" class="smallList">7</option>
            </select>
            <select name="o2" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
                <option value="2" class="smallList">2</option>
                <option value="3" class="smallList">3</option>
                <option value="4" class="smallList">4</option>
                <option value="5" class="smallList">5</option>
                <option value="6" class="smallList">6</option>
                <option value="7" class="smallList">7</option>
            </select>
            <select name="o3" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
                <option value="2" class="smallList">2</option>
                <option value="3" class="smallList">3</option>
                <option value="4" class="smallList">4</option>
                <option value="5" class="smallList">5</option>
                <option value="6" class="smallList">6</option>
                <option value="7" class="smallList">7</option>
            </select>
            <select name="o4" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
                <option value="2" class="smallList">2</option>
                <option value="3" class="smallList">3</option>
                <option value="4" class="smallList">4</option>
                <option value="5" class="smallList">5</option>
                <option value="6" class="smallList">6</option>
                <option value="7" class="smallList">7</option>
            </select>
            <br><label>Binary: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
            <select name="b1" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
            </select>
            <select name="b2" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
            </select>
            <select name="b3" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
            </select>
            <select name="b4" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
            </select>
            <select name="b5" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
            </select>
            <select name="b6" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
            </select>
            <select name="b7" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
            </select>
            <select name="b8" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
            </select>
            <select name="b9" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
            </select>
            <select name="b10" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
            </select>
            <select name="b11" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
            </select>
            <br><label>Hexadecimal:&nbsp;&nbsp; </label>
            <select name="h1" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
                <option value="2" class="smallList">2</option>
                <option value="3" class="smallList">3</option>
                <option value="4" class="smallList">4</option>
                <option value="5" class="smallList">5</option>
                <option value="6" class="smallList">6</option>
                <option value="7" class="smallList">7</option>
                <option value="8" class="smallList">8</option>
                <option value="9" class="smallList">9</option>
                <option value="A" class="smallList">A</option>
                <option value="B" class="smallList">B</option>
                <option value="C" class="smallList">C</option>
                <option value="D" class="smallList">D</option>
                <option value="E" class="smallList">E</option>
                <option value="F" class="smallList">F</option>
            </select>
            <select name="h2" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
                <option value="2" class="smallList">2</option>
                <option value="3" class="smallList">3</option>
                <option value="4" class="smallList">4</option>
                <option value="5" class="smallList">5</option>
                <option value="6" class="smallList">6</option>
                <option value="7" class="smallList">7</option>
                <option value="8" class="smallList">8</option>
                <option value="9" class="smallList">9</option>
                <option value="A" class="smallList">A</option>
                <option value="B" class="smallList">B</option>
                <option value="C" class="smallList">C</option>
                <option value="D" class="smallList">D</option>
                <option value="E" class="smallList">E</option>
                <option value="F" class="smallList">F</option>
            </select>
            <select name="h3" class="texts">
                <option value="0" class="smallList">0</option>
                <option value="1" class="smallList">1</option>
                <option value="2" class="smallList">2</option>
                <option value="3" class="smallList">3</option>
                <option value="4" class="smallList">4</option>
                <option value="5" class="smallList">5</option>
                <option value="6" class="smallList">6</option>
                <option value="7" class="smallList">7</option>
                <option value="8" class="smallList">8</option>
                <option value="9" class="smallList">9</option>
                <option value="A" class="smallList">A</option>
                <option value="B" class="smallList">B</option>
                <option value="C" class="smallList">C</option>
                <option value="D" class="smallList">D</option>
                <option value="E" class="smallList">E</option>
                <option value="F" class="smallList">F</option>
            </select>
            <br>
        </div>

        <div class="vp"> <input type="image" src="Images/Level1/NextLevelSign.png" alt="Submit"> </div>
    </form>
    <div class="thumb">
        <a>
            <span><input type="image" src="Images/Level1/PuzzlesTemplatesQM.png" onclick="alerter()">
            </span>
        </a>
    </div>


    <div class="bg">
    </div>
    <div class="helpText">
        HELP!
    </div>
    <div class="challenge">
        <span>Introduction to Informatics<br>Challenge by Professor: <br>Yves Le Traon</span>
    </div>
    <div class="levelNumber">
        &nbsp;Level 01&nbsp;
    </div>
</body>

</html>