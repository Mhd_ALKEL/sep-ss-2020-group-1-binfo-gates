<?php
include 'gameSession.php';

// set the number of your variable here
$level = 2;

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    //endGame();
    $gameSession = retrieveGameSession();
    $gameSession->startLevel($level);
} else {
    try {
        $accuracy = 0;
        $splittedAnswer = explode(",", $_POST['answer']);
        for ($i = 0; $i < count($splittedAnswer); $i++) {
            if (strval($i + 1) == $splittedAnswer[$i]) {
                $accuracy += 30;
            }
        }
    } catch (Exception $e) {
        echo "cheater";
        return;
    }

    submitScore($level, $accuracy, "");
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="puzzle" content="width=device-width, initial-scale=1.0">
    <title>Level 2</title>
    <!-- development version, includes helpful console warnings -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/vue/2.5.2/vue.min.js"></script>
    <!-- CDNJS :: Sortable (https://cdnjs.com/) -->
    <script src="//cdn.jsdelivr.net/npm/sortablejs@1.8.4/Sortable.min.js"></script>
    <!-- CDNJS :: Vue.Draggable (https://cdnjs.com/) -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.20.0/vuedraggable.umd.min.js"></script>
    <link href="template.css" rel="stylesheet" type="text/css">
    <style>
        .body {
            background-image: url("Images/Prog.jpg");
            background-size: cover;
        }

        .ide {
            position: absolute;
            top: 19%;
            left: 47%;
            height: 420px;
            width: 570px;
            border-style: groove;
            border-color: black;
            background-image: url("Images/IDE.png");
            background-size: cover;
        }

        .content {
            position: absolute;
            top: 48%;
            left: 57%;
            color: #afb1b3;
            height: 200px;
            width: 200px;
            font-family: "Century";
            font-size: 20px;
            line-height: 30px;
        }
        .notes {
            position: absolute;
            top: 19%;
            left: 47%;
            height: 420px;
            width: 570px;
            border-style: groove;
            border-color: black;
            background-image: url("Images/IDE.png");
            background-size: cover;
        }
        .codeBlocks {
            position: absolute;
            background-color: #2b2b2b;
            top: 19%;
            left: 5%;
            height: 420px;
            width: 570px;
            border-style: groove;
            border-color: blueviolet;
            font-family: "Century";
            font-size: 28px;
            color: #afb1b3;
            text-align: center;
            alignment: center;
        }

        .smallbox {
            height: 200px;
            width: 250px;
            border-color: firebrick;
            border-style: ridge;
            color: azure;
            position: absolute;
            top: 39%;
            left: 28%;
        }

        .items {
            border-color: firebrick;
            border-style: ridge;
        }
    </style>
</head>
<script>
    function alerter() {
        if (alert('The right order is: \nvar i = 0; \ni *= 2; \ni = i == 0 ? 10 : 5; \nprint(i);')) {
            zeroing();
        } else {
            zeroing();
        }
    }

    function zeroing() {
        let accuracy = 0;
        window.location.replace('submitScore.php?level=2&accuracy=' + accuracy + '&message=You used helping button so your points here are zero');
    }
</script>

<body class="body">
    <div class="ide">
    </div>
    <div id="app">
        <div class="codeBlocks">
            Drag these pieces of code in the right order <br>to the IDE window
            in order to <br> get an output of 10.<br>
            ----------------------
            <draggable class="smallbox" :list="toSolve" group="code">
                <div class="list-group-item" v-for="item in toSolve" :key="item.id">
                    <div class="items">{{ item.text }}</div>
                </div>
            </draggable>
        </div>
        <div>
            <draggable class="content" :list="attempt" group="code" @change="onUpdate">
                <div class="list-group-item" v-for="item in attempt" :key="item.id">
                    {{ item.text }}
                </div>
            </draggable>
        </div>
        <form action="level2.php" method="POST">
            <input type="text" name="answer" id="answer" hidden="true">
            <button class="vp"> <img src="Images/Level1/NextLevelSign.png"> </button>
        </form>

    </div>
    <div class="helpText">
        HELP!
    </div>
    <div class="thumb">
        <a href="#">
            <span><img src="Images/Level1/PuzzlesTemplatesQM.png" onclick="alerter()"></span>
        </a>
    </div>
    <div class="challenge">
        <span>Logic of Programming<br>Challenge by Professor: <br>Denis Zampunieris</span>
    </div>
    <div class="levelNumber">
        &nbsp;Level 02&nbsp;
    </div>
    <script src="scripts/level2.js" type="module">
    </script>
</body>

</html>