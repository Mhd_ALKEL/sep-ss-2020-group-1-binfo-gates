<?php
include 'gameSession.php';
include 'dbconfig.php';

// retrieve gamesessions
$gameSession = retrieveGameSession();
if ($gameSession->areLevelsFinished() == false) {
    header("Location: level{$gameSession->getCurrentLevel()}.php");
    exit;
}

// calculate total score
$nickname = $gameSession->getUsername();
$totalScore = 0;
foreach ($gameSession->getLevelResults() as $levelResult) {
    $totalScore += $levelResult->getScore();
}

// // add to the database
 try {
//     // connection to the db
   $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
     // request
     $sql = "INSERT INTO leaderboard(username, score)
            VALUES('$nickname', $totalScore)";
     $q = $conn->exec($sql);

    if ($q == false) {
        die("Could not connect proced to a query to the database $dbname :");
     }
 } catch (PDOException $pe) {
    die("Could not connect to the database $dbname :" . $pe->getMessage());
 }

// end session
endGame();

?>
<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Results page</title>
</head>
<style>
    body {
        width: 100%;
        height: 100%;
        margin: 0;
        display: contents;
        min-width: 1470px;
        min-height: 740px;
    }

    .bg {
        height: 100%;
        width: 100%;
        margin: 0 auto;
        position: static;
        min-width: 1470px;
        min-height: 740px;
        background-image: url("Images/TheEnd.jpg");
        background-size: cover;
        font-size: 150px;
        font-family: Chiller;
        text-align: center;
        color: #0031dc;
        display: block;
    }
    .longList{
        position: absolute;
        left: 0%;
        top: 0%;
        font-size: 35px;
        text-align: center;
    }
    table,
    th,
    td {
        border: 0px solid transparent;
        border-collapse: collapse;
    }
    th,
    td {
        padding: 5px;
        text-align: left;
    }
</style>

<body>


    <ol class="bg">
        <br> Your Score is: <font color="#b22222"> <?php
                                                    echo $totalScore;
                                                    ?></font>
        <table class="longList">
            <thead>
            <tr>
                <th><font color="#b22222">Level&nbsp;</font></th>
                <th><font color="#b22222">&nbsp;&nbsp;Score&nbsp;&nbsp;</font></th>
            </tr>
            </thead>
            <tbody><tr>
            <?php $current = 1;
            foreach ($gameSession->getLevelResults() as $levelResult) : ?>

                    <td><font color="green"><?php echo $current++; ?></font></td>
                    <td><?php echo $levelResult->getScore(); ?></td>
                    <?php ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </ol>

</body>

</html>