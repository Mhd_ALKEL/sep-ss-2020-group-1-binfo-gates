<?php
include 'dbconfig.php';

try {
    // connection to the db
    $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    // request
    $sql = 'SELECT username, score
        FROM leaderboard
        ORDER BY score DESC
        LIMIT 10';
    $q = $conn->query($sql);

    if ($q == false) {
        die("Could not connect proced to a query to the database $dbname :");
    }

    $q->setFetchMode(PDO::FETCH_ASSOC);
} catch (PDOException $pe) {
    die("Could not connect to the database $dbname :" . $pe->getMessage());
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <table class="table table-bordered table-condensed">
        <thead>
            <tr>
                <th>Username</th>
                <th>Score</th>

            </tr>
        </thead>
        <tbody>
            <?php while ($r = $q->fetch()) : ?>
                <tr>
                    <td><?php echo htmlspecialchars($r['username']) ?></td>
                    <td><?php echo htmlspecialchars($r['score']); ?></td>
                </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
</body>

</html>