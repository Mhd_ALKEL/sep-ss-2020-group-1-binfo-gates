<?php

include 'gameSession.php';

$level = 10;


if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    //endGame();
    $gameSession = retrieveGameSession();
    $gameSession->startLevel($level);
} else {

    $accuracy = 0;
    submitScore($level, $accuracy, "");
}

?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Level 10</title>
    <link href="template.css" rel="stylesheet" type="text/css">
    <style>
        body {
            background-image: url('Images/underDesk.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100% 100%;
        }
        .screen{
            position: absolute;
            left: 2%;
            top: 20%;
        }
        .blueScreen{
            position: absolute;
            left: 3.1%;
            top: 21.85%;
            background-image: url("Images/Level10/blueScreen.png");
            height: 283px;
            width: 565px;
            background-size: 100% 100%;
        }
        .text{
            position: absolute;
            top: 0%;
            left: 27%;
            font-size: 25px;
            color: black;
            font-family: cursive;
        }
        .components{
            position: absolute;
            top:35%;
            left: 45%;
        }
        </style>
    <script>
        let accuracy = 0;
        let clicked1 = false;
        let clicked2 = false;
        let clicked3 = false;
        let clicked4 = false;
        let clicked5 = false;
        function endLevel() {
            if(clicked1 === false && clicked2 === true && clicked3 === false && clicked4 === false && clicked5 === false){
                accuracy = 120;
            }
            else {
                accuracy = 0;
            }
            window.location.replace('submitScore.php?level=10&accuracy=' + accuracy)
        }
        function alerter(){
            if (alert('You need to choose the M.2 Nvme ssd "The tiny black rectangular device "')) {
                zeroing();
            }
            else{
                zeroing();
            }
        }
        function zeroing() {
            accuracy = 0;
            window.location.replace('submitScore.php?level=10&accuracy=' + accuracy + '&message=You used helping button so your points here are zero');
        }
        function entourage(current) {
            let item = document.getElementById(current);
            if(current === "psr"){
                if(clicked1 === false) {
                    item.src = "Images/Level10/processorc.png";
                    clicked1 = true;
                }
                else{
                    item.src = "Images/Level10/processor.jpg";
                    clicked1 = false;
                }
            }
            if(current === "m2"){
                if(clicked2 === false){
                item.src = "Images/Level10/m.2c.png";
                clicked2 = true;
            }
            else{
                item.src = "Images/Level10/m.2.png";
                clicked2 = false;
            }
            }
            if(current === "psu"){
                if(clicked3 === false) {
                    item.src = "Images/Level10/psuc.png";
                    clicked3 = true;
                }
                else{
                    item.src = "Images/Level10/psu.png";
                    clicked3 = false;
                }
            }
            if(current === "clr"){
                if(clicked4 === false){
                item.src = "Images/Level10/coolerc.png";
                clicked4 = true;
                }
                else {
                    item.src = "Images/Level10/cooler.png";
                    clicked4 = false;
                }
            }
            if(current === "gpu"){
                if(clicked5 === false){
                item.src = "Images/Level10/gpuc.png";
                clicked5 = true;
                }
                else {
                    item.src = "Images/Level10/gpu.png";
                    clicked5 = false;
                }
            }
        }
    </script>
</head>
<body>
<div class="text">You get the BlueScreen in your PC when you try to run it<br>
after booting when it starts Windows it crashes and give you this screen. <br>
There's another component on your PC that's not working<br>
click on this component.</div>
<div class="components"><img id="psr" src="Images/Level10/processor.jpg" height="75" width="75" onclick="entourage(this.id)">
<img id="m2" src="Images/Level10/m.2.png" height="35" width="120" onclick="entourage(this.id)">
    <img id="psu" src="Images/Level10/psu.png" height="250" width="250" onclick="entourage(this.id)">
    <img id="clr" src="Images/Level10/cooler.png" height="275" width="250" onclick="entourage(this.id)" ><br>
<img id="gpu" src="Images/Level10/gpu.png" height="200" width="400" onclick="entourage(this.id)">
</div>
<div class="screen"><img src="Images/Level10/screen.png" height="400" width="600"></div><div class="blueScreen"></div>
<div class="vp" > <button onclick="endLevel()"><img src="Images/Level1/NextLevelSign.png">
    </button></div>
<div class="helpText">
    HELP!
</div>
<div class="thumb">
    <a href="#">
        <span><img src="Images/Level1/PuzzlesTemplatesQM.png" onclick="alerter()"></span>
    </a>
</div>
<div class="challenge">
    <span>Daily life<br>Sudden Technical problem 2<br>software and hardware</span>
</div>
<div class="levelNumber">
    &nbsp;Level 10&nbsp;
</div>
</body>
</html>