var app = new Vue({
  el: '#app',
  data: {
    toSolve: [
      { id: 1, text: 'var i = 0;' },
      { id: 2, text: 'i *= 2;' },
      { id: 3, text: 'i = i == 0 ? 10 : 5;' },
      { id: 4, text: 'print(i);' }
    ],
    attempt: [],
    problem: 'Output must be 10'
  },
  methods: {
    onUpdate: function () {
      var answer = "";
      for (var i = 0; i < this.attempt.length; i++) {
        if (answer.length > 0)
          answer += ','
        answer += this.attempt[i].id
      }
      document.getElementById("answer").value = answer
    }
  }
})

// randomise the problem
app.toSolve.sort(() => Math.random() - 0.5)

function endLevel(accury) {
  window.location.replace('submitScore.php?level=2&accuracy=' + accury)
}