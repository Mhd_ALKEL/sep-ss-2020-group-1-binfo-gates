var app = new Vue({
    el: '#app',
    data: {
        toSolve: [
            { id: 1, text: 'Images/equation1.png' },
            { id: 2, text: 'Images/equation2.png' },
            { id: 3, text: 'Images/equation3.png' },
            { id: 4, text: 'Images/equation4.png' },
            { id: 5, text: 'Images/equation5.png' },
            { id: 6, text: 'Images/equation6.png' }
        ],
        attempt: [],
        problem: 'Calculus'
    },
    methods: {
        onUpdate: function () {
            var answer = "";
            for (var i = 0; i < this.attempt.length; i++) {
                if (answer.length > 0)
                    answer += ','
                answer += this.attempt[i].id
            }
            document.getElementById("answer").value = answer
        }
    }
})

// randomise the problem
app.toSolve.sort(() => Math.random() - 0.5)
