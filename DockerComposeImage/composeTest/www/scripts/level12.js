var app = new Vue({
  el: '#app',
  data: {
    toSolve: [
      { id: 1, text: 'p = q;' },
      { id: 2, text: '*p = 2;' },
      { id: 3, text: 'q = p;' },
      { id: 4, text: 'q++;' },
      { id: 5, text: 'char *t = x;' },
      { id: 6, text: 'x = y;' },
      { id: 7, text: 'y = t;' },
      { id: 8, text: 'y = x;' }
    ],
    attempt1: [],
    attempt2: [],
    problem1: 'Output must be 0 2',
    problem2: 'Output must be programing binfo'
  },
  methods: {
    onUpdate1: function () {
      var answer = "";
      for (var i = 0; i < this.attempt1.length; i++) {
        if (answer.length > 0)
          answer += ','
        answer += this.attempt1[i].id
      }
      document.getElementById("answer1").value = answer
    },
    onUpdate2: function () {
      var answer = "";
      for (var i = 0; i < this.attempt2.length; i++) {
        if (answer.length > 0)
          answer += ','
        answer += this.attempt2[i].id
      }
      document.getElementById("answer2").value = answer
    }
  }
})

// randomise the problem
app.toSolve.sort(() => Math.random() - 0.5)