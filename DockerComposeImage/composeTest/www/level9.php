<?php

include 'gameSession.php';

$level = 9;


if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    //endGame();
    $gameSession = retrieveGameSession();
    $gameSession->startLevel($level);
} else {

    $accuracy = 0;
    submitScore($level, $accuracy, "");
}

?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Level 9</title>
    <link href="template.css" rel="stylesheet" type="text/css">
    <style>
        body {
            background-image: url('Images/underDesk.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100% 100%;
        }
        .mother{
            position: absolute;
            top:35%;
            left: 55%;
            height: 45%;
            width: 30%;
        }
        .rams{
            position: absolute;
            top: 30%;
            left: 05%;
        }
        .text{
            position: absolute;
            top: 0%;
            left: 27%;
            font-size: 25px;
            color: black;
            font-family: cursive;
        }
        .reset{
            position: absolute;
            top: 55%;
            left: 87%;
            background-color: black;
            color: azure;
            height: 6%;
            width: 8%;
            border-style: solid;
            border-color: grey;
            font-size: 40px;
            text-combine: true;
        }
        .DDR2{
            position: absolute;
            top: 39%;
            left: 79%;
        }
        .DDR4-1{
            position: absolute;
            top: 39%;
            left:79.8%;
        }
        .DDR3{
            position: absolute;
            top: 39%;
            left: 80.7%;
        }
        .DDR4-2{
            position: absolute;
            top: 39%;
            left: 81.5%;
        }
        .info{
            position: absolute;
            top: 80%;
            left: 15%;
            font-size: 25px;
        }
    </style>
    <script>
        let accuracy = 0;
        let chosen1 = false;
        let chosen2 = false;
        let chosen3 = false;
        let chosen4 = false;
        function DDR2(current) {
           // document.getElementById(current).remove();
            document.getElementById(current).style.visibility = 'hidden';
            let img = document.createElement("img");
            img.class="DDR2";
            img.id = "i1";
            img.src = "Images/DDR2-800r.png";
            img.height = 197;
            img.width = 11;
            document.getElementById("D2-8").appendChild(img);
            chosen1 = true;
        }
        function DDR3(current) {
            document.getElementById(current).style.visibility = 'hidden';
            let img = document.createElement("img");
            img.class="DDR3";
            img.id = "i2";
            img.src = "Images/DDR3-2666r.png";
            img.height = 197;
            img.width = 11;
            document.getElementById("D3-26").appendChild(img);
            chosen2 = true;
        }
        function DDR4_1(current) {
            document.getElementById(current).style.visibility = 'hidden';
            let img = document.createElement("img");
            img.class="DDR4-1";
            img.id = "i3";
            img.src = "Images/DDR4-2666r.png";
            img.height = 197;
            img.width = 11;
            document.getElementById("D4-26").appendChild(img);
            chosen3 = true;
        }
        function DDR4_2(current) {
            document.getElementById(current).style.visibility = 'hidden';
            let img = document.createElement("img");
            img.class="DDR4-2";
            img.id = "i4";
            img.src = "Images/DDR4-3000r.png";
            img.height = 197;
            img.width = 11;
            document.getElementById("D4-30").appendChild(img);
            chosen4 = true;
        }
        function reset() {
            if(document.getElementById("r1").style.visibility === 'hidden'){
                document.getElementById("r1").style.visibility = 'initial';
                document.getElementById("i1").remove();
            }
            if(document.getElementById("r2").style.visibility === 'hidden'){
                document.getElementById("r2").style.visibility = 'initial';
                document.getElementById("i2").remove();
            }
            if(document.getElementById("r3").style.visibility === 'hidden'){
                document.getElementById("r3").style.visibility = 'initial';
                document.getElementById("i3").remove();
            }
            if(document.getElementById("r4").style.visibility === 'hidden'){
                document.getElementById("r4").style.visibility = 'initial';
                document.getElementById("i4").remove();
            }
            chosen1 = false;
            chosen2 = false;
            chosen3 = false;
            chosen4 = false;
        }
        function endLevel() {
            if(chosen1 === false && chosen2 === false && chosen3 === true && chosen4 === true){
                accuracy = 120;
            }
            else {
                accuracy = 0;
            }
            window.location.replace('submitScore.php?level=9&accuracy=' + accuracy)
        }
        function alerter(){
            if (alert('You need to choose the DDR4 rams only')) {
                zeroing();
            }
            else{
                zeroing();
            }
        }
        function zeroing() {
            accuracy = 0;
            window.location.replace('submitScore.php?level=9&accuracy=' + accuracy + '&message=You used helping button so your points here are zero');
        }
        function info(current) {
            if(current === "r1"){
                document.getElementById("t").innerHTML = "DDR2 Memory Frequency is: 800Mhz Capacity: 8GB";
            }
            if(current === "r2"){
                document.getElementById("t").innerHTML = "DDR3 Memory Frequency is: 2666Mhz Capacity: 8GB";
            }
            if(current === "r3"){
                document.getElementById("t").innerHTML = "DDR4 Memory Frequency is: 2666Mhz Capacity: 8GB";
            }
            if(current === "r4"){
                document.getElementById("t").innerHTML = "DDR4 Memory Frequency is: 3000Mhz Capacity: 8GB";
            }
            document.getElementById("t").style.visibility = 'initial';
        }
        function hide(current) {
            document.getElementById("t").style.visibility = 'hidden';
        }
    </script>
</head>
<body>
<div class="text">Your computer has a very serious problem, your Power supply was very bad<br>
and it burned your RAM memory<br> Now you need to replace them by exactly 2 of the following sticks<br>NOTE:
you can't choose only one! you need to install two of them that could be compatible<br>
You can know more information about the sticks by placing your courser on them<br>
    Click on the stick to install it, There's another hint in the Motherboard "be smart!"<br>
If you need to reset, click on the button on the right</div>
<div class="mother"><img src="Images/Motherboard.png" height="450" width="550"></div>
<div class="DDR3" id="D3-26"></div>
<div class="DDR4-1" id="D4-26"></div>
<div class="DDR4-2" id="D4-30"></div>
<div class="DDR2" id="D2-8"></div>
<div class="rams">
    <img id="r1" src="Images/DDR2-800.png" width="300" height="150" onclick="DDR2(this.id)" onmouseover="info(this.id)" onmouseleave="hide(this.id)">
    <img id="r2" src="Images/DDR3-2666.png" width="300" height="150" onclick="DDR3(this.id)" onmouseover="info(this.id)" onmouseleave="hide(this.id)"><br>
    <img id="r3" src="Images/DDR4-2666.png" width="300" height="150" onclick="DDR4_1(this.id)" onmouseover="info(this.id)" onmouseleave="hide(this.id)">
    <img id="r4" src="Images/DDR4-3000.png" width="300" height="150" onclick="DDR4_2(this.id)" onmouseover="info(this.id)" onmouseleave="hide(this.id)">
</div>
<div class="reset" onclick="reset()">RESET</div>
<div class="info" ><p id="t" style="visibility: hidden"></p></div>
<div class="vp" > <button onclick="endLevel()"><img src="Images/Level1/NextLevelSign.png">
    </button></div>
<div class="helpText">
    HELP!
</div>
<div class="thumb">
    <a href="#">
        <span><img src="Images/Level1/PuzzlesTemplatesQM.png" onclick="alerter()"></span>
    </a>
</div>
<div class="challenge">
    <span>Daily life<br>Sudden Technical problem 1<br>Hardware</span>
</div>
<div class="levelNumber">
    &nbsp;Level 09&nbsp;
</div>
</body>
</html>