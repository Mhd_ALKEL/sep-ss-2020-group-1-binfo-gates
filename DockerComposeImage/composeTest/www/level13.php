<?php

include 'gameSession.php';

$level = 13;


if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    //endGame();
    $gameSession = retrieveGameSession();
    $gameSession->startLevel($level);
} else {

    $accuracy = 0;
    $message = "";
    submitScore($level, $accuracy, $message);
}

?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Level 13</title>
    <link href="template.css" rel="stylesheet" type="text/css">
    <style>
        body {
            background-image: url('Images/lastLevel.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100% 100%;
        }
        .text{
            position: absolute;
            top: 20%;
            left: 0%;
            width: 35%;
            font-size: 25px;
            color: white;
            font-family: cursive;
        }
        </style>
    <script>
        let accuracy = 0;
        function endLevel() {
            accuracy = 120;
            window.location.replace('submitScore.php?level=13&accuracy=' + accuracy)
        }
        function alerter(){
            if (alert('First: right click on your mouse \n Second: Choose inspect element \n Third: search for (div class="vp")' +
                '\n Forth: change (visibility: hidden) to (visibility: initial) ')) {
                zeroing();
            }
            else{
                zeroing();
            }
        }
        function zeroing() {
            accuracy = 0;
            let message = "You used helping button so Your points here are zero";
            window.location.replace('submitScore.php?level=13&accuracy=' + accuracy + '&message=You used helping button so your points here are zero');
        }
    </script>
</head>
<body>
<div class="text">Congratulation! You are now in the Last level, you made it! <br><br>
BUT Wait! There's no next level button, now you need to find a solution to this problem<br>
The hint is web programming it's very simple to do but maybe hard to discover</div>
<div class="vp"  style="visibility: hidden"> <button onclick="endLevel()"><img src="Images/Level1/NextLevelSign.png">
    </button></div>
<div class="helpText">
    HELP!
</div>
<div class="thumb" >
    <a href="#">
        <span><img src="Images/Level1/PuzzlesTemplatesQM.png" onclick="alerter()"></span>
    </a>
</div>
<div class="challenge">
    <span>Last challenge<br>Web Programming<br>Prof: Volker Muller </span>
</div>
<div class="levelNumber">
    &nbsp;Level 13&nbsp;
</div>
</body>
</html>