<?php

include 'gameSession.php';

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $levelStr = $_GET['level'];
    $accuracyStr = $_GET['accuracy'];
    $message = "";

    if (is_numeric($levelStr) && is_numeric($accuracyStr)) {
        if (is_string($_GET['message'])) {
            $message = $_GET['message'];
        }

        $level = intval($levelStr);
        $accuracy = floatval($accuracyStr);

        $gameSession = retrieveGameSession();
        $gameSession->endLevel($level, $accuracy, $message);

        if ($gameSession->areLevelsFinished() == false) {
            header("Location: level{$gameSession->getCurrentLevel()}.php");
        } else {
            header("Location: resultPage.php");
        }
    }
}
