<?php

include 'gameSession.php';

$accuracy = 0;
function calculate($value)
{
    $accuracy = $value;
    if ($_POST["c1"] == "Data-Link") {
        $accuracy += 15;
    }
    if ($_POST["c2"] == "Data-Link") {
        $accuracy += 15;
    }
    if ($_POST["c3"] == "Application") {
        $accuracy += 15;
    }
    if ($_POST["c4"] == "Application") {
        $accuracy += 15;
    }
    if ($_POST["c5"] == "Transport") {
        $accuracy += 15;
    }
    if ($_POST["c6"] == "Network") {
        $accuracy += 15;
    }
    if ($_POST["c7"] == "Application") {
        $accuracy += 15;
    }
    if ($_POST["c8"] == "Transport") {
        $accuracy += 15;
    }
    return $accuracy;
}
$level = 11;

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $gameSession = retrieveGameSession();
    $gameSession->startLevel($level);
} else {
    $accuracy = calculate($accuracy);
    submitScore($level, $accuracy, "");
}

?>
<!DOCTYPE HTML>
<html>

<head>
    <title>Level 11</title>
    <link href="template.css" rel="stylesheet" type="text/css">
    <style>
        body {
            background-image: url('Images/Level11/Wall.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100% 100%;
        }

        .texts {
            font-family: "digital";
            font-size: 35px;
            text-align: center;
            background-color: black;
            color: aqua;
            -moz-appearance: textfield;
        }

        .InitialTexts {
            font-family: "digital";
            font-size: 36px;
            text-align: center;
            background-color: black;
            color: seagreen;
            -moz-appearance: textfield;
        }

        .placing {
            position: absolute;
            left: 5%;
            top: 45%;
        }

        .initialPlacing {
            position: absolute;
            left: 5%;
            top: 25%;
        }

        .text {
            position: absolute;
            top: 75%;
            left: 15%;
            font-size: 35px;
            color: black;
            font-family: cursive;
        }

        vp {
            position: absolute;
            bottom: 1.8%;
            right: 1.8%;
            border-style: groove;
            border-color: darkgrey;
            border-width: 5px;
        }

        .vp input {
            transition: transform .5s ease-in-out;
        }

        .vp:hover input {
            transform: scale(1) rotate(25deg);
        }
    </style>
    <script>
        function alerter() {
            if (alert('Application: FTP , HTTP , SMTP \n Transport: TCP , UDP \n Network: IP \n Data-Link: ARP , DHCP')) {
                zeroing();
            } else {
                zeroing();
            }
        }

        function zeroing() {
            let accuracy = 0;
            window.location.replace('submitScore.php?level=11&accuracy=' + accuracy + '&message=You used helping button so your points here are zero');
        }
    </script>
</head>

<body>
    <div class="text">Choose the responsible layer for each of the listed protocols above </div>
    <div class="initialPlacing">
        <input class="InitialTexts" value="ARP" size="10" readonly />
        <input class="InitialTexts" value="DHCP" size="10" readonly />
        <input class="InitialTexts" value="FTP" size="10" readonly />
        <input class="InitialTexts" value="HTTP" size="10" readonly />
        <input class="InitialTexts" value="UDP" size="10" readonly />
        <input class="InitialTexts" value="IP" size="10" readonly />
        <input class="InitialTexts" value="SMTP" size="10" readonly />
        <input class="InitialTexts" value="TCP" size="10" readonly />
    </div>
    <form method="post" action="level11.php">
        <div class="placing">
            <select name="c1" class="texts">
                <option value="?" class="smallList" selected>?</option>
                <option value="Application" class="smallList">Application</option>
                <option value="Transport" class="smallList">Transport</option>
                <option value="Network" class="smallList">Network</option>
                <option value="Data-Link" class="smallList">Data-Link</option>
            </select>
            <select name="c2" class="texts">
                <option value="?" class="smallList" selected>?</option>
                <option value="Application" class="smallList">Application</option>
                <option value="Transport" class="smallList">Transport</option>
                <option value="Network" class="smallList">Network</option>
                <option value="Data-Link" class="smallList">Data-Link</option>
            </select>
            <select name="c3" class="texts">
                <option value="?" class="smallList" selected>?</option>
                <option value="Application" class="smallList">Application</option>
                <option value="Transport" class="smallList">Transport</option>
                <option value="Network" class="smallList">Network</option>
                <option value="Data-Link" class="smallList">Data-Link</option>
            </select>
            <select name="c4" class="texts">
                <option value="?" class="smallList" selected>?</option>
                <option value="Application" class="smallList">Application</option>
                <option value="Transport" class="smallList">Transport</option>
                <option value="Network" class="smallList">Network</option>
                <option value="Data-Link" class="smallList">Data-Link</option>
            </select>
            <select name="c5" class="texts">
                <option value="?" class="smallList" selected>?</option>
                <option value="Application" class="smallList">Application</option>
                <option value="Transport" class="smallList">Transport</option>
                <option value="Network" class="smallList">Network</option>
                <option value="Data-Link" class="smallList">Data-Link</option>
            </select>
            <select name="c6" class="texts">
                <option value="?" class="smallList" selected>?</option>
                <option value="Application" class="smallList">Application</option>
                <option value="Transport" class="smallList">Transport</option>
                <option value="Network" class="smallList">Network</option>
                <option value="Data-Link" class="smallList">Data-Link</option>
            </select>
            <select name="c7" class="texts">
                <option value="?" class="smallList" selected>?</option>
                <option value="Application" class="smallList">Application</option>
                <option value="Transport" class="smallList">Transport</option>
                <option value="Network" class="smallList">Network</option>
                <option value="Data-Link" class="smallList">Data-Link</option>
            </select>
            <select name="c8" class="texts">
                <option value="?" class="smallList" selected>?</option>
                <option value="Application" class="smallList">Application</option>
                <option value="Transport" class="smallList">Transport</option>
                <option value="Network" class="smallList">Network</option>
                <option value="Data-Link" class="smallList">Data-Link</option>
            </select>
        </div>
        <div class="vp"><input type="image" src="Images/Level1/NextLevelSign.png" alt="Submit"> </div>
    </form>
    <div class="helpText">
        HELP!
    </div>
    <div class="thumb">
        <a href="#">
            <span><img src="Images/Level1/PuzzlesTemplatesQM.png" onclick="alerter()"></span>
        </a>
    </div>
    <div class="challenge">
        <span>Networks<br>Challenge by Professor: <br>TIKHOMIROV Sergei</span>
    </div>
    <div class="levelNumber">
        &nbsp;Level 1 1&nbsp;
    </div>
</body>

</html>