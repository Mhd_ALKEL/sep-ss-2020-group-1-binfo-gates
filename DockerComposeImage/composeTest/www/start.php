<?php
include 'gameSession.php';


// end game one exists
endGame();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (is_string($_POST['username']) && startGame($_POST['username'])) {
        $gameSession = retrieveGameSession();
        header("Location: level{$gameSession->getCurrentLevel()}.php");
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body {font-family: Arial, Helvetica, sans-serif;
            background-color: black;}
        form {border: 3px solid #f1f1f1;}

        input[type=text], input[type=password] {
            width: 100%;

            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #000000;
            box-sizing: border-box;
        }

        button {
            background-color: #0031dc;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }

        button:hover {
            opacity: 0.8;
        }

        .imgcontainer {
            text-align: center;
            margin: 24px 0 12px 0;
        }

        img.avatar {
            width: 45%;
        }

        .container {
            padding: 16px;
        }

        span.psw {
            float: right;
            padding-top: 16px;
        }
    </style>
</head>
<body>
    <form action="start.php" method="post">
        <div class="imgcontainer">
            <img src="Images/uni.jpg" alt="Avatar" class="avatar">
        </div>
        <div class="container">
            <input type="text" placeholder="Enter Your name" name="username" required>

            <button type="submit">Start</button>
        </div>
    </form>

    </body>
</body>

</html>