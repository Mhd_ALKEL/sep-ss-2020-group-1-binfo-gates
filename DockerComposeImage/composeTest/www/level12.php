<?php

include 'gameSession.php';

$level = 12;


if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    //endGame();
    $gameSession = retrieveGameSession();
    $gameSession->startLevel($level);
} else {

    $accuracy = 0;
    submitScore($level, $accuracy, "");
}

?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Level 12</title>
    <link href="template.css" rel="stylesheet" type="text/css">
    <style>
        body {
            background-image: url('Images/ITLevel.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100% 100%;
        }
        table{
            border-style: solid;
            border-width: 7px;
            border-color: black;
        }
        td{
            border-width: 0px;
            border-color: black;
            width: 900px;
            height: 60px;
            border-style: solid;
            border-width: 7px;
            border-color: black;
            background-color: black;
            color: white;
            text-align: center;
            font-family: cursive;
            font-size: 28px;
        }
        .tableLocation{
            position: absolute;
            top: 0%;
            left: 25%;
            height: 85%;
            width: 70%;
        }
        .reset{
            position: absolute;
            top: 50%;
            left: 90%;
            background-color: red;
            color: azure;
            height: 6%;
            width: 8%;
            border-style: solid;
            border-color: grey;
            font-size: 35px;
            text-combine: true;
        }
        .writing{
            position: absolute;
            top: 25%;
            left: 0%;
            background-color: white;
            color: black;
            border-style: solid;
            border-width: 8px;
            border-color: blue;
            width: 22%;
            font-family: cursive;
            font-size: 18px;
        }
        </style>
    <script>
        let accuracy = 0;
        function choosing(current){
            document.getElementById(current).style.visibility = 'hidden';
            if(current === "s1"){
                accuracy += 20;
            }
            if(current === "s2"){
                accuracy += 20;
            }
            if(current === "s4"){
                accuracy += 20;
            }
            if(current === "s6"){
                accuracy += 20;
            }
            if(current === "s7"){
                accuracy += 20;
            }
            if(current === "s9"){
                accuracy += 20;
            }
            if(current === "s3"){
                accuracy -=40;
            }
            if(current === "s5"){
                accuracy -=40;
            }
            if (current === "s8"){
                accuracy -=40;
            }
        }
        function reset(){
            for(let i = 1 ; i < 10 ; i++){
                let returner = "s" + i.toString();
                document.getElementById(returner).style.visibility = 'initial';
            }
            accuracy = 0;
        }
        function endLevel() {

            window.location.replace('submitScore.php?level=12&accuracy=' + accuracy)
        }
        function alerter(){
            if (alert('The Three statements that are right: \n ' +
                'Cutting off electricity while flashing your BIOS will completely destroy your motherboard \n ' +
                'GPUs are the most important parts for medical researches \n' +
                'The original name of Windows was Interface Manager ')) {
                zeroing();
            }
            else{
                zeroing();
            }
        }
        function zeroing() {
            accuracy = 0;
            window.location.replace('submitScore.php?level=12&accuracy=' + accuracy + '&message=You used helping button so your points here are zero');
        }
    </script>
</head>
<body>
<div class="writing">When you are an IT student you will listen to many lies and myths about IT and Computer science in general<br>
    So you have 9 statements here<br> 6 of them are not true or not possible<br> you need to eliminate these statements by clicking on them.
    <br>Only 3 of them are true, you need to keep them and then go to the next level.<br>
    You can reset your choices by clicking on the red button on the right.
</div>
<button class="reset"  onclick="reset()">RESET</button>
<div class="tableLocation"><table id ="t">
    <tr>
        <td id="s1" onclick="choosing(this.id)">Macs do not get viruses</td>
    </tr>
    <tr>
        <td id="s2" onclick="choosing(this.id)">Shutting down the PC is very important to make it last longer</td>
    </tr>
        <tr>
            <td id="s3" onclick="choosing(this.id)">Cutting off electricity while flashing your BIOS will completely destroy your motherboard</td>
        </tr>
    <tr>
        <td id="s4" onclick="choosing(this.id)">Keeping your phone charging during the night will hurt the battery</td>
    </tr>
        <tr>
            <td id="s5" onclick="choosing(this.id)">GPUs are the most important parts for medical researches</td>
        </tr>
    <tr>
        <td id="s6" onclick="choosing(this.id)">Transistor sizes are measured by Picometer nowadays</td>
    </tr>
    <tr>
        <td id="s7" onclick="choosing(this.id)">8 Core CPU with low frequency and IPC is faster than 4 Core CPU with high frequency and IPC</td>
    </tr>
    <tr>
        <td id="s8" onclick="choosing(this.id)">The original name of Windows was Interface Manager</td>
    </tr>
    <tr>
        <td id="s9" onclick="choosing(this.id)">Computer science is all about programming</td>
    </tr></table>
</div>
<div class="vp" > <button onclick="endLevel()"><img src="Images/Level1/NextLevelSign.png">
    </button></div>
<div class="helpText">
    HELP!
</div>
<div class="thumb">
    <a href="#">
        <span><img src="Images/Level1/PuzzlesTemplatesQM.png" onclick="alerter()"></span>
    </a>
</div>
<div class="challenge">
    <span>Daily Life<br>General IT Question<br>Random People</span>
</div>
<div class="levelNumber">
    &nbsp;Level 12&nbsp;
</div>
</body>
</html>