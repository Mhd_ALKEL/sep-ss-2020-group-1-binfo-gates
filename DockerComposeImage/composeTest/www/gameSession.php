<?php

session_start();

function retrieveGameSession()
{
    if (isset($_SESSION['GameSession']) == false) {
        die("Game session not started");
    }

    return $_SESSION['GameSession'];
}

function startGame(string $username)
{
    if (isset($_SESSION['GameSession']) == false) {
        $_SESSION['GameSession'] = new GameSession($username);
        return true;
    }

    return false;
}

function endGame()
{
    if (isset($_SESSION['GameSession'])) {
        unset($_SESSION['GameSession']);
        return true;
    }

    return false;
}

function submitScore(int $level, float $accuracy, string $message)
{
    $gameSession = retrieveGameSession();
    $gameSession->endLevel($level, $accuracy, $message);

    if ($gameSession->areLevelsFinished() == false) {
        header("Location: level{$gameSession->getCurrentLevel()}.php");
    } else {
        header("Location: resultPage.php");
    }
}

class GameSession
{
    const LEVELS_NUMBER = 13;

    private $username;
    private $currentLevel;
    private $currentLevelStartTime;
    private $levelStarted;
    private $levelResults;

    function __construct(string $username)
    {
        $this->username = $username;
        $this->currentLevel = 1;
        $this->currentLevelStartTime = new DateTime();
        $this->levelResults = array();
    }

    function getUsername()
    {
        return $this->username;
    }

    function getCurrentLevel()
    {
        return $this->currentLevel;
    }

    function getLevelScore(int $level)
    {
        if (0 < $level && $level <= count($this->levelResults)) {
            return $this->levelResults[$level - 1];
        }

        return -1;
    }

    function getLevelResults()
    {
        return $this->levelResults;
    }

    function areLevelsFinished()
    {
        return $this->currentLevel > GameSession::LEVELS_NUMBER;
    }

    function startLevel(int $level)
    {
        if ($this->areLevelsFinished()) {
            header("Location: resultPage.php");
        } else {
            if ($level != $this->currentLevel) {
                header("Location: level{$this->currentLevel}.php");
            } else if ($this->levelStarted == false) {
                $this->currentLevelStartTime = new DateTime();
                $this->levelStarted = true;
                return true;
            }
        }

        return false;
    }

    function endLevel(int $level, float $accuracy, string $message)
    {
        if ($this->areLevelsFinished()) {
            header("Location: resultPage.php");
        } else if ($level == $this->currentLevel) {
            $timeTaken = $this->currentLevelStartTime->diff(new DateTime());
            $minutesTaken = $timeTaken->m; // minutes used to resolve the puzzle            
            $score = $accuracy * (1 - 0.10 * $minutesTaken);

            $levelResult = new LevelResult($score, $timeTaken->s, $message);
            array_push($this->levelResults, $levelResult);

            if ($this->areLevelsFinished() == false) {
                $this->currentLevel++;
            }

            return true;
        }

        return false;
    }
}

class LevelResult
{
    private $score;
    private $timeTaken;
    private $message;

    function __construct(float $score, int $timeTaken, string $message)
    {
        $this->score = $score;
        $this->timeTaken = $timeTaken;
        $this->message = $message;
    }

    function getScore()
    {
        return $this->score;
    }

    function getTimeTaken()
    {
        return $this->timeTaken;
    }

    function getMessage()
    {
        return $this->message;
    }
}
