<?php

include 'gameSession.php';

// set the number of your variable here
$level = 6;

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    //endGame();
    $gameSession = retrieveGameSession();
    $gameSession->startLevel($level);
} else {
    $accuracy = 0;
    $description = "";

    // first prbl
    if ($_POST['firstA'] == 0) {
        $accuracy += 30;
    }

    // second prbl
    if ($_POST['secondA'] == 1) {
        $accuracy += 30;
    }

    // third prbl
    if ($_POST['thirdA1'] == 0 && $_POST['thirdA2'] == 1) {
        $accuracy += 60;
    }

    submitScore($level, $accuracy, "");
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Level 6</title>
    <link href="template.css" rel="stylesheet" type="text/css">
</head>
<style>
    body {
        background-image: url("Images/mathe.jpg");
        background-size: cover;
        overflow: auto;
        width: 100%;
        height: 100%;
    }

    .back {
        position: absolute;
        top: 2%;
        left: 25%;
        height: 82%;
        width: 60%;
        border-style: groove;
        border-color: black;
        background-color: black;
        color: azure;
        font-family: "Kozuka Gothic Pro B";
        font-size: 30px;
    }

    .texts {
        font-family: "digital";
        font-size: 40px;
        text-align: center;
        background-color: black;
        color: aqua;
        -moz-appearance: textfield;
    }

    .InitialTexts {
        font-family: "digital";
        font-size: 40px;
        text-align: center;
        background-color: black;
        color: seagreen;
    }
    table {
        position: absolute;
        border-collapse: collapse;
        border: 2px solid rgb(200, 200, 200);
        letter-spacing: 1px;
        height: 100px;
        width: 600px;
        font-family: "Calibri";
        font-size: 30px;
        text-align: center;
        background-color: black;
        color: #1a2c16;
        -moz-appearance: textfield;
    }
    .t1{
        position: absolute;
        top: 20%;
        left: 18%;
    }
    .t2{
        position: absolute;
        top: 45%;
        left: 18%;
    }
    .t3{
        position: absolute;
        top: 70%;
        left: 18%;
    }
    td,
    th {
        border: 1px solid rgb(190, 190, 190);
        padding: 10px 20px;
    }

    th {
        background-color: black;
    }

    td {
        text-align: center;
    }

    tr:nth-child(even) td {
        background-color: black;
        font-family: digital;
    }

    tr:nth-child(odd) td {
        background-color: grey;

    }

    caption {
        padding: 10px;
    }
    vp {
        position: absolute;
        bottom: 1.8%;
        right: 1.8%;
        border-style: groove;
        border-color: darkgrey;
        border-width: 5px;
    }

    .vp input {
        transition: transform .5s ease-in-out;
    }

    .vp:hover input {
        transform: scale(1) rotate(25deg);
    }
</style>
<script>
    function alerter() {
        if (alert('The answers: First table: 0 \n Second table: 1 \n Third table: 0 | 1')) {
            zeroing();
        } else {
            zeroing();
        }
    }

    function zeroing() {
        let accuracy = 0;
        window.location.replace('submitScore.php?level=6&accuracy=' + accuracy + '&message=You used helping button so your points here are zero');
    }
</script>
<body>
   <form action="" method="POST">
       <div class="back">Fill the Missing numbers (In blue) in these Truth tables below.
        <table class="t1">
            <tr>
                <td>A</td>
                <td>B</td>
                <td>C</td>
                <td>A && B || C</td>
            </tr>
            <tr>
                <td><input class="InitialTexts" value="0" maxlength="1" size="1" readonly /></td>
                <td><input class="InitialTexts" value="1" maxlength="1" size="1" readonly /></td>
                <td><input class="InitialTexts" value="0" maxlength="1" size="1" readonly /></td>
                <td>
                    <select class="texts" name="firstA">
                        <option value=0>0</option>
                        <option value=1>1</option>
                    </select>
                </td>
            </tr>
        </table>

        <table class="t2">
            <tr>
                <td>A</td>
                <td>B</td>
                <td>C</td>
                <td>A && B && C</td>
            </tr>
            <tr>
                <td><input class="InitialTexts" value="1" maxlength="1" size="1" readonly /></td>
                <td><input class="InitialTexts" value="1" maxlength="1" size="1" readonly /></td>
                <td>
                    <select  class="texts" name="secondA">
                        <option value=0>0</option>
                        <option value=1>1</option>
                    </select>
                </td>
                <td><input class="InitialTexts" value="0" maxlength="1" size="1" readonly /></td>
            </tr>
        </table>

        <table class="t3">
            <tr>
                <td>A</td>
                <td>B</td>
                <td>C</td>
                <td>(!A && C ) && (B || C)</td>
            </tr>
            <tr>
                <td>
                    <select class="texts" name="thirdA1">
                        <option value=0>0</option>
                        <option value=1>1</option>
                    </select>
                </td>
                <td><input class="InitialTexts" value="1" maxlength="1" size="1" readonly /></td>
                <td>
                    <select  class="texts" name="thirdA2">
                        <option value=0>0</option>
                        <option value=1>1</option>
                    </select>
                </td>
                <td><input class="InitialTexts" value="1" maxlength="1" size="1" readonly /></td>
            </tr>
        </table> </div>
        <div class="vp"><input type="image" src="Images/Level1/NextLevelSign.png" alt="Submit"></div>
  </form>
        <div class="helpText">
            HELP!
        </div>
        <div class="thumb">
            <a href="#">
                <span><img src="Images/Level1/PuzzlesTemplatesQM.png" onclick="alerter()"></span>
            </a>
        </div>
        <div class="challenge">
            <span>Discrete Mathematics<br>Challenge by Professor: <br>Bruno Teheux</span>
        </div>
        <div class="levelNumber">
            &nbsp;Level 06&nbsp;
        </div>
    </form>
</body>

</html>